import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_archiver_appliance_service_running_and_enabled(host):
    service = host.service('archiver-appliance')
    assert service.is_running
    assert service.is_enabled


def test_tomcat_services_running_and_enabled(host):
    for instance in ('mgmt', 'engine', 'etl', 'retrieval'):
        service = host.service('tomcat@' + instance)
        assert service.is_running
        assert service.is_enabled


def test_tomcat_instances_access(host):
    # There is no database server available so the appliance won't start properly
    cmd = host.run('curl -L http://localhost:17665/mgmt/ui/index.html')
    assert 'The requested service is not currently available.' in cmd.stdout
    # Check that each instance url can be accessed
    for url in ('http://localhost:17667/etl/bpl',
                'http://localhost:17668/retrieval/bpl',
                'http://localhost:17666/engine/bpl'):
        cmd = host.run('curl {}'.format(url))
        assert 'The requested resource is not available.' in cmd.stdout
