ics-ans-role-epicsarchiverap
============================

Ansible role to install [EPICS Archiver Appliance](http://slacmshankar.github.io/epicsarchiver_docs/index.html) on CentOS 7.3.

This role doesn't take care of creating the database and required tables.
You should use the ics-ans-role-mariadb for that. See the ics-ans-archappl playbook.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
# EPICS Archiver Appliance release and url
epicsarchiverap_release: "archappl_v0.0.1_SNAPSHOT_22-June-2017T14-44-56"
epicsarchiverap_url: "https://artifactory.esss.lu.se/artifactory/swi-pkg/archiver-appliance/v0.0.1_SNAPSHOT_22-June-2017/{{ epicsarchiverap_release }}.tar.gz"

# root directory where the archiver appliance release will be unarchived
epicsarchiverap_release_root: /opt
# root location of the short, medium and long term stores
epicsarchiverap_storage: /home/archappl

# mysql/mariadb parameters
epicsarchiverap_db_name: archappl
epicsarchiverap_db_user: archappl
epicsarchiverap_db_password: archappl
epicsarchiverap_db_url: mysql://localhost:3306

# single appliance id or cluster appliance prefix
epicsarchiverap_id: appliance0

# single server hostname or IP used in appliance.xml
# set to hostname by default - can be forced if required
epicsarchiverap_host: "{{ ansible_hostname }}"

# the host group to get hosts for the cluster, enables clustering
# when this variable is changed from default, it disables the
# epicsarchiverap_host variable above
epicsarchiverap_cluster: ""

# Tomcat instances HTTP connector port
epicsarchiverap_mgmt_port: 17665
epicsarchiverap_engine_port: 17666
epicsarchiverap_etl_port: 17667
epicsarchiverap_retrieval_port: 17668

# root directory where tomcat instances are installed
epicsarchiverap_tomcats_base: "/var/lib/tomcats"

# JAVA options
epicsarchiverap_java_maxmetaspace: "256M"
epicsarchiverap_java_heapsize: "512M"

# EPICS variables
epicsarchiverap_epics_base: /srv/epics/bases/base-3.15.4
epicsarchiverap_epics_libs_archive: https://artifactory.esss.lu.se/artifactory/ansible-provision/epics-libs/3.15.4/centos7-x86_64/epics_libs.tgz
# EPICS_CA_ADDR_LIST will only be defined if this variable is not empty
epicsarchiverap_epics_ca_addr_list: ""
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epicsarchiverap
```

License
-------

BSD 2-clause

The following templates come from the official Apache Tomcat installation and are included under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.txt):

- templates/context.xml.j2
- templates/server.xml.j2
- templates/tomcat-users.xml.j2
- templates/web.xml.j2

A local copy of the licenses can be found in the files LICENSE and Apache_2.0_License.txt.
